# u06-restaurant

## Name
U06 resturangapp

## Description
Beställ mat från meny, sök maträtter via text och flags samt möjlighet att ta bort maträtter från beställningen.  


# SQLite
Funktionerna är kopplade till databas. Se filen code för att se tables och seeds.

# Testning
Koden körs via pipeline med code_quality. För pylint, autopep8 och flake8. 

# Visuals
## Restaurant prompt displays with choices and ordernumber. 

Your ordernumber is:
 282258
----THE RESTAURANT----
Please make a choice!
1. Meny
2. Search and order dish by number
3. Search and order dish by text
4. Delete dish from order
5. Search from flag
6. Exit
Your selection:

## See the menu:

Your selection:1
1.Hawaii 117kr
Skinka ananas

2.Funghi 117kr
Champinjoner

3.Tropicana 112kr
Skinka banan curry ananas

4.Ostburgare 129kr
Ost, tomat, dressing
...

## Search dish with text and order. 
Your selection:3
Search dish with text?TEx
The dish is:
7.Texasburgare 135kr

Would you like to order Texasburgare? Y or N:Y
How many would you like to order?1
Notes to the chef:Extra ost
Din beställning:
236924: Texasburgare 135kr 1st 
Extra ost

## Search with flag
Your selection:5
Search flag?Veg

2.Funghi 117kr

5.Halloumiburgare 129kr

10.Mozzarellasallad 135kr

# Improvements sprint 1
-Skapa unittest för funktionerna i koden. 
-Slå ihop flags och searchdish för färre menyval. 
-Det blir fel om man lägger in samma rätt på samma ordernummer i två sessioner. 
-Vissa queries går inte att lägga utanför menyn. {} och ? fattar jag inte riktigt. 


# Improvements Sprint 2
-Summera kostnad för rätter
-Skapa pdf-kvitto för beställning
-Personalen behöver kunna ta bort maträtter från menyn när den är slutsåld
-Gör relevanta unittest för funktionerna
-Gör interface https://github.com/bczsalba/pytermgui
-Skapa dockerfile

# Buggar/småfix
-När man först beställer en maträtt sen beställer en till så blir det knas. 
-Kommer inte in någon data till table order, då kommer inte timestamp med t.ex. Just nu är beställningen kopplad till order_dishes. Det kanske är fel? Borde kanske vara till order?
-Pylint klagar på viss kod. Har lagt ignore på W0621 eftersom jag tycker att den ska ignoreras men det verkar inte funka ändå.
-När man beställer via nummer på maträtten så borde man få felmeddelande om man t.ex. fyller i en bokstav. Likadant om man söker via namn och flags på maträtt samt delete. 

# Förslag på unittest
- att man får felmeddelande om man trycker fel eller en maträtt som inte finns. 
- att korrekt maträtt visas vid de olika sökningarna. 
- att beställningen sammanfattar vad som faktiskt lagts i kundkorgen. Även efter delete.
-  



# Project status

