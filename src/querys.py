'''
Resturangen
'''

import random
import sqlite3

# Queries som variabler för att kunna hämta dem till funktioner


GET_ALL_DISHES = 'SELECT * FROM dishes;'

SEARCH_NUMBER = "SELECT id, name, price FROM dishes WHERE id = ?;"

SEARCH_TEXT = "SELECT id, name, price FROM dishes WHERE name LIKE '%{}%';"

# NEW='''SELECT id, name, price FROM dishes WHERE name LIKE ? OR id = ?;'''

FLAGS_S = '''SELECT id, name, price
from dish_flags
Join dishes on dishes.id = dish_flags.dish
WHERE flags LIKE '%{}%';'''

SHOW_ORDER = ''' SELECT name, price, ordername, quantity, notes
from order_dishes
JOIN dishes on dishes.id = order_dishes.dish
WHERE ordername = '{}';'''

SHOW_ORDER_ID = ''' SELECT id, name, quantity
from order_dishes
JOIN dishes on dishes.id = order_dishes.dish
WHERE ordername = '{}';'''

SQL2 = '''DELETE from order_dishes where ordername = ? AND dish = ?;'''

MENU_PROMPT = """----THE RESTAURANT----
Please make a choice!
1. Meny
2. Search and order dish by number
3. Search and order dish by text
4. Delete dish from order
5. Search from flag
6. Exit
Your selection:"""


conn = sqlite3.connect('data.sql')
cur = conn.cursor()


def connect():
    return sqlite3.connect('data.sql')


ordernumber = random.randint(100000, 999999)


def menu():
    print("\nYour ordernumber is:\n", ordernumber)
    while (user_input := input(MENU_PROMPT)) != "6":
        if user_input == "1":
            def get_dishes(conn):
                '''Hämta menyn från query'''
                with conn:
                    return conn.execute(GET_ALL_DISHES).fetchall()
            dishes = get_dishes(conn)
            for dish in dishes:
                print(f"{dish[0]}.{dish[1]} {dish[3]}kr\n{dish[2]}\n")
        elif user_input == "2":
            def search_by_number(conn):
                '''Sök via nummer genom placeholder query.
                dishes används för att stoppa in inputen i queryn'''
                with conn:
                    return conn.execute(search_by_number).fetchall()
            number = input("whats the number?")
            var = cur.execute(SEARCH_NUMBER, number)
            for dish in var:
                order_input = input(f'''The dish is:\n{dish[0]}.{dish[1]} {dish[2]}kr

Would you like to order {dish[1]}? Y or N:''')

                if order_input == "Y":
                    quantity = input("How many would you like to order?")
                    to_chef = input("Notes to the chef:")
                    o_d_q = ordernumber, dish[0], to_chef, quantity
                    sql = '''INSERT INTO order_dishes VALUES(?,?,?,?)'''
                    cur.execute(sql, o_d_q)
                    conn.commit()
                    print("Din beställning:")
                    s_order = SHOW_ORDER.format(ordernumber)
                    data2 = cur.execute(s_order)

                    for row in data2:
                        print(f"{row[2]}: {row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n")
                    conn.commit()
                elif order_input == "N":
                    print("okidoki")
                else:
                    print("invalid input:")
        elif user_input == "3":
            def search_dish(conn):
                '''Sök via nummer genom placeholder query.
                dishes används för att stoppa in inputen i queryn'''
                with conn:
                    return conn.execute(search_dish).fetchall()

            text = input("Search dish with text?")
            dishes = SEARCH_TEXT.format(text)
            var = cur.execute(dishes)
            for dish in var:

                order_input_text = input(f'''The dish is:\n{dish[0]}.{dish[1]} {dish[2]}kr

Would you like to order {dish[1]}? Y or N:''')

                if order_input_text == "Y":
                    '''
                    Om man vill beställa så får skapas fler input som ingår i
                    variabel samt ordernummer för att sen skicka in i queryn.
                    Sen har jag valt att göra en ny query för vad som visas.
                    '''
                    quantity = input("How many would you like to order?")
                    to_chef = input("Notes to the chef:")
                    o_d_q = ordernumber, dish[0], to_chef, quantity
                    sql = '''INSERT INTO order_dishes VALUES(?,?,?,?)'''

                    cur.execute(sql, o_d_q)
                    conn.commit()
                    print("Din beställning:")

                    s_order = SHOW_ORDER.format(ordernumber)
                    data2 = cur.execute(s_order)

                    for row in data2:
                        print(f"{row[2]}: {row[0]} {row[1]}kr {row[3]}st \n{row[4]}\n")
                    conn.commit()
                elif order_input_text == "N":
                    print("okidoki")
                else:
                    print("invalid input:")

        elif user_input == "4":
            def delete_dish(conn):
                with conn:
                    return conn.execute(delete_dish)

            print("Din beställning:")

            s_order_id = SHOW_ORDER_ID.format(ordernumber)

            data2 = cur.execute(s_order_id)

            for row in data2:
                print(f"{row[0]}.{row[1]} {row[2]}st\n")
            conn.commit()

            delete_input = input("Vilken rätt vill du ta bort från beställningen:")
            delete_var = ordernumber, delete_input

            cur.execute(SQL2, delete_var)
            conn.commit()
            print("Din beställning:")

            s_order_id = SHOW_ORDER_ID.format(ordernumber)
            data2 = cur.execute(s_order_id)

            for row in data2:
                print(f"{row[0]}.{row[1]} {row[2]}st\n")
            conn.commit()

        elif user_input == "5":
            def search_flag(conn):
                with conn:
                    return conn.execute(search_flag).fetchall()

            text = input("Search flag?")
            dishes = FLAGS_S.format(text)
            var = cur.execute(dishes)

            for dish in var:
                print(f"\n{dish[0]}.{dish[1]} {dish[2]}kr")
        else:
            print("Invalid input try again!")


menu()


conn.close()
